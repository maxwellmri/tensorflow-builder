die() {
  # Print a message and exit with code 1.
  #
  # Usage: die <error_message>
  #   e.g., die "Something bad happened."

  echo $@
  exit 1
}

/tensorflow/configure

CONTAINER_TYPE=$( echo "$1" | tr '[:upper:]' '[:lower:]' )
shift 1

MAVX_FLAG=""
while true; do
  if [[ "${1}" == "--test_tutorials" ]]; then
    DO_TEST_TUTORIALS=1
  elif [[ "${1}" == "--integration_tests" ]]; then
    DO_INTEGRATION_TESTS=1
  elif [[ "${1}" == "--mavx" ]]; then
    MAVX_FLAG="--copt=-mavx"
  elif [[ "${1}" == "--mavx2" ]]; then
    MAVX_FLAG="--copt=-mavx2"
  fi

  shift
  if [[ -z "${1}" ]]; then
    break
  fi
done

PIP_BUILD_TARGET="//tensorflow/tools/pip_package:build_pip_package"
GPU_FLAG=""
if [[ ${CONTAINER_TYPE} == "cpu" ]]; then
  bazel build -c opt ${MAVX_FLAG} ${PIP_BUILD_TARGET} || \
      die "Build failed."
elif [[ ${CONTAINER_TYPE} == "gpu" ]]; then
  bazel build -c opt --config=cuda ${MAVX_FLAG} ${PIP_BUILD_TARGET} || \
      die "Build failed."
  GPU_FLAG="--gpu"
else
  die "Unrecognized container type: \"${CONTAINER_TYPE}\""
fi

mkdir /out/${CONTAINER_TYPE}
bazel-bin/tensorflow/tools/pip_package/build_pip_package /out/${CONTAINER_TYPE} || \
    die "build_pip_package FAILED"
